#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>
#include <stdio.h>
#include <strings.h>

int main(int argc, char **argv)
{
    //struct mq_attr attr;
    //attr.

    mqd_t mqd = mq_open("/test.mq", O_RDWR | O_CREAT, 0777, NULL); // возможно, надо O_NONBLOCK
    
    mq_attr attr;
    mq_getattr(mqd, &attr);
    printf("mq_msgsize: %d\n", attr.mq_msgsize);
    
    char buffer[attr.mq_msgsize + 1];
    bzero(buffer, attr.mq_msgsize + 1);

    if (! mq_receive(mqd, buffer, attr.mq_msgsize + 1, NULL)) {
            perror("mq_receive");
            return 1;
    }
    
    FILE *fd = fopen("/home/box/message.txt", "a");
    fputs(buffer, fd);
    fclose(fd);
    mq_close(mqd);

    return 0;
}

